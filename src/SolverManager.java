import java.nio.file.Files;
import java.nio.file.Paths;

public class SolverManager {

    private final SolverController solverController;
    private String data;

    public SolverManager() {
        solverController = new SolverController();
    }

    public static SolverManager newInstance() {
        return new SolverManager();
    }

    public void solve(String path) {
        solve(path, Runtime.getRuntime().availableProcessors()/2+1);
    }

    public void solve(String path, int threadsAmount) {
        data = getDataFromFile(path);
        if (data == null) {
            return;
        }
        Solver[] solvers = getSolvers(threadsAmount);
        Thread[] threads = new Thread[threadsAmount];
        for (int i = 0; i < threadsAmount; i++) {
            threads[i] = new Thread(solvers[i]);
        }
        for (int i = 0; i < threadsAmount; i++) {
            threads[i].start();
        }
        for (int i = 0; i < threadsAmount; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Solver getSolver(int order, int total) {
        return new Solver(this.solverController, this.data, order, total);
    }

    private Solver[] getSolvers(int amount) {
        Solver[] solvers = new Solver[amount];
        for (int i = 0; i < amount; i++) {
            solvers[i] = getSolver(i, amount);
        }
        return solvers;
    }

    private String getDataFromFile(String path) {
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(path));
            return new String(bytes);
        } catch (Exception e) {
            System.out.println("Specified path is invalid");
            return null;
        }
    }
}
