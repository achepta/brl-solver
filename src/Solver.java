public class Solver implements Runnable {

    private final SolverController solverController;

    private final String[] data;
    private final int depth;
    private final int[][] table;
    private final int[][][] pieces;
    private final int[][][] binaryPatterns;
    private final int[] iValuesByStep;
    private final int[] jValuesByStep;
    private final int[][] possiblePiecesByStep;
    private final int[][] finalPiecesByStep;
    private final int rows;
    private final int columns;
    private final int piecesAmount;
    private final int threadSplitPoint;

    private final int order;
    private final int total;

    public Solver(SolverController solverController, String data, int order, int total) {
        this.solverController = solverController;
        this.data = data.replaceAll("\r", "").split("\n");
        this.order = order;
        this.total = total;
        this.depth = getDepthFromData();
        this.threadSplitPoint = depth - 1;
        this.table = getTableFromData();
        this.rows = table.length;
        this.columns = table[0].length;
        this.pieces = getPiecesFromData();
        this.piecesAmount = pieces.length;
        this.binaryPatterns = generateAllBinaryPatterns();
        this.iValuesByStep = new int[rows * columns];
        this.jValuesByStep = new int[rows * columns];
        this.possiblePiecesByStep = new int[rows * columns][piecesAmount];
        this.finalPiecesByStep = new int[rows * columns][piecesAmount];

        fillAllIJValues();
        fillPossiblePiecesByStep();
        fillFinalPiecesByStep();
    }

    @Override
    public void run() {
        solve();
    }

    public void solve() {
        try {
            makeStep(0, new int[piecesAmount]);
        } catch (RuntimeException e) {
            solverController.flag = true;
            String result = e.getMessage();
            if (!result.isEmpty() && !solverController.isPrinted) {
                solverController.isPrinted = true;
                System.out.println(result);
            }
        }
    }

    private void makeStep(int stepId, int[] usedPieces) throws RuntimeException {
        if (solverController.flag) {
            throw new RuntimeException("");
        }
        int currentI = iValuesByStep[stepId];
        int currentJ = jValuesByStep[stepId];
        putNeededPieces(usedPieces, stepId);
        int[] possiblePieces = getPossiblePieces(usedPieces, stepId);
        int[] targets = getTargetsToReach(currentI, currentJ, usedPieces, possiblePieces);
        if (targets.length > 0) {
            int bits = possiblePieces.length;
            if (stepId == threadSplitPoint) {
                for (int i = order; i < binaryPatterns[bits].length; i += total) {
                    processSequence(stepId, usedPieces, possiblePieces, targets, binaryPatterns[bits][i]);
                }
            } else {
                for (int i = 0; i < binaryPatterns[bits].length; i++) {
                    processSequence(stepId, usedPieces, possiblePieces, targets, binaryPatterns[bits][i]);
                }
            }
        }
    }

    private void processSequence(int stepId, int[] usedPieces, int[] possiblePieces, int[] targets, int[] sequence) {
        int checker = isSequenceValid(sequence, targets, possiblePieces, usedPieces);
        if (checker != -1) {
            int[] usedPiecesForNextStep = getNewUsedPieces(sequence, possiblePieces, usedPieces, stepId);
            if (checker == 0) {
                if (isTableSolved(usedPiecesForNextStep)) {
                    throw new RuntimeException(createResultFromUsedPieces(usedPiecesForNextStep));
                } else {
                    return;
                }
            }
            makeStep(stepId + 1, usedPiecesForNextStep);
        }
    }

    private boolean isTableSolved(int[] usedPieces) {
        // using some bitwise magic instead of modulus operator
        // https://lustforge.com/2016/05/08/modulo-operator-performance-impact/
        if (depth == 2) {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if ((getCurrentCellValue(i, j, usedPieces) & 1) == 1) {
                        return false;
                    }
                }
            }
        } else if (depth == 4) {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if ((getCurrentCellValue(i, j, usedPieces) & 3) != 0) {
                        return false;
                    }
                }
            }
        } else {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    if (((int) (getCurrentCellValue(i, j, usedPieces) * 0x55555556L >> 30) & 3) != 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private int[] getNewUsedPieces(int[] bin, int[] possiblePieces, int[] usedPieces, int stepId) {
        int[] newUsedPieces = new int[usedPieces.length];
        System.arraycopy(usedPieces, 0, newUsedPieces, 0, usedPieces.length);
        for (int i = 0; i < bin.length; i++) {
            if (bin[i] == 1) {
                newUsedPieces[possiblePieces[i]] = 1 + stepId;
            }
        }
        return newUsedPieces;
    }

    private int isSequenceValid(int[] sequence, int[] targets, int[] possiblePieces, int[] usedPieces) {
        int sum = 0;
        int usedCount = 0;
        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] == 1) {
                if (usedPieces[possiblePieces[i]] == 0) {
                    sum += pieces[possiblePieces[i]][0][0];
                    usedCount++;
                } else {
                    return -1;
                }
            }
        }
        for (int i = 0; i < targets.length; i++) {
            if (sum == targets[i]) {
                int leftPiecesCount = 0;
                for (int j = 0; j < piecesAmount; j++) {
                    if (usedPieces[j] == 0) {
                        leftPiecesCount++;
                    }
                }
                return leftPiecesCount - usedCount;
            }
        }
        return -1;
    }

    private int[] getTargetsToReach(int currentI, int currentJ, int[] usedPieces, int[] possiblePieces) {
        int currentCellValue = getCurrentCellValue(currentI, currentJ, usedPieces);
        int maxValue = 0;
        for (int i = 0; i < possiblePieces.length; i++) {
            maxValue += pieces[possiblePieces[i]][0][0];
        }
        int amountOfTargets = maxValue / depth + 1;
        int[] targets = new int[amountOfTargets];
        int leftToDepth = (depth - currentCellValue % depth) % depth;
        for (int i = 0; i < amountOfTargets; i++) {
            targets[i] = leftToDepth + depth * i;
            if (amountOfTargets == 1 && targets[i] > maxValue) {
                return new int[0];
            }
        }
        return targets;
    }

    private int getCurrentCellValue(int currentI, int currentJ, int[] usedPieces) {
        int usedSum = 0;
        for (int i = 0; i < piecesAmount; i++) {
            if (usedPieces[i] != 0) {
                usedSum += getPieceCellValue(i, usedPieces[i], currentI, currentJ);
            }
        }
        return table[currentI][currentJ] + usedSum;
    }

    private int getPieceCellValue(int pieceId, int stepWhenUsed, int currentI, int currentJ) {
        stepWhenUsed = stepWhenUsed - 1;
        int usedI = iValuesByStep[stepWhenUsed];
        int usedJ = jValuesByStep[stepWhenUsed];
        int[][] usedPiece = pieces[pieceId];
        if (usedI + usedPiece.length - 1 >= currentI && usedJ + usedPiece[0].length - 1 >= currentJ
                && usedI <= currentI && usedJ <= currentJ) {
            return usedPiece[currentI - usedI][currentJ - usedJ];
        }
        return 0;
    }

    private void putNeededPieces(int[] usedPieces, int stepId) {
        for (int i = 0; i < piecesAmount; i++) {
            if (usedPieces[i] == 0 && finalPiecesByStep[stepId][i] == 1) {
                usedPieces[i] = 1 + stepId;
            }
        }
    }

    private boolean isFinalPlaceForPiece(int currentI, int currentJ, int[][] availablePiece) {
        return currentI + availablePiece.length == rows
                && currentJ + availablePiece[0].length == columns;
    }

    private int[] getPossiblePieces(int[] usedPieces, int stepId) {
        int possibleCount = 0;
        for (int i = 0; i < piecesAmount; i++) {
            if (usedPieces[i] == 0 && possiblePiecesByStep[stepId][i] == 1) {
                possibleCount += 1;
            }
        }
        int[] result = new int[possibleCount];
        int insertCount = 0;
        for (int i = 0; i < piecesAmount; i++) {
            if (usedPieces[i] == 0 && possiblePiecesByStep[stepId][i] == 1) {
                result[insertCount] = i;
                insertCount++;
            }
        }
        return result;
    }

    private boolean isPossiblePlaceForPiece(int currentI, int currentJ, int[][] availablePiece) {
        return currentI + availablePiece.length <= rows
                && currentJ + availablePiece[0].length <= columns;
    }


    //    parse data
    private int getDepthFromData() {
        return Integer.parseInt(data[0]);
    }

    private int[][] getTableFromData() {
        String[] commaSplit = data[1].split(",");
        return getMatrixFromCommaSplitArray(commaSplit);
    }

    private int[][][] getPiecesFromData() {
        String[] spaceSplit = data[2].split(" ");
        int[][][] pieces = new int[spaceSplit.length][][];
        for (int i = 0; i < spaceSplit.length; i++) {
            String[] commaSplit = spaceSplit[i]
                    .replaceAll("\\.", "0")
                    .replaceAll("X", "1")
                    .split(",");
            pieces[i] = getMatrixFromCommaSplitArray(commaSplit);
        }
        return pieces;
    }

    private int[][] getMatrixFromCommaSplitArray(String[] rows) {
        int[][] table = null;
        for (int i = 0; i < rows.length; i++) {
            String[] digitsSplit = rows[i].split("");
            for (int j = 0; j < digitsSplit.length; j++) {
                if (i == 0 && j == 0) {
                    table = new int[rows.length][digitsSplit.length];
                }
                if (table != null) {
                    table[i][j] = Integer.parseInt(digitsSplit[j]);
                }
            }
        }
        return table;
    }


    // utils

    private int[][][] generateAllBinaryPatterns() {
        int[][][] binaryPatterns = new int[piecesAmount + 1][][];
        for (int i = 0; i < piecesAmount + 1; i++) {
            int iterations = 1 << i;
            int[][] binaryArraysContainer = new int[iterations][];
            int offset = 0;
            for (int j = 0; j < i + 1; j++) {
                offset = generateAllBinaryPatternsByLength(i, new int[i], j, 0, binaryArraysContainer, offset);
            }
            binaryPatterns[i] = binaryArraysContainer;
        }
        return binaryPatterns;
    }

    private int generateAllBinaryPatternsByLength(int lengthOfPattern, int[] seed, int numberOfOnes, int currentCount, int[][] containerToFill, int offset) {
        if (lengthOfPattern == 0) {
            if (currentCount == numberOfOnes) {
                containerToFill[offset] = seed.clone();
                offset++;
                return offset;
            }
            return offset;
        }
        for (int j = 0; j < 2; j++) {
            seed[lengthOfPattern - 1] = j;
            int currentCountTemp = j == 1 ? (currentCount + 1) : (currentCount);

            if (currentCountTemp > numberOfOnes) {
                return offset;
            }
            offset = generateAllBinaryPatternsByLength(lengthOfPattern - 1, seed, numberOfOnes, currentCountTemp, containerToFill, offset);

        }
        return offset;
    }

    private void fillAllIJValues() {
        for (int i = 0; i < rows * columns; i++) {
            iValuesByStep[i] = i / columns;
            jValuesByStep[i] = i % columns;
        }
    }

    private void fillPossiblePiecesByStep() {
        for (int i = 0; i < rows * columns; i++) {
            for (int j = 0; j < piecesAmount; j++) {
                possiblePiecesByStep[i][j] = isPossiblePlaceForPiece(iValuesByStep[i], jValuesByStep[i], pieces[j]) ? 1 : 0;
            }
        }
    }

    private void fillFinalPiecesByStep() {
            for (int i = 0; i < rows * columns; i++) {
                for (int j = 0; j < piecesAmount; j++) {
                    finalPiecesByStep[i][j] = isFinalPlaceForPiece(iValuesByStep[i], jValuesByStep[i], pieces[j]) ? 1 : 0;
                }
            }
        }

    private String createResultFromUsedPieces(int[] usedPieces) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < piecesAmount; i++) {
            int stepId = usedPieces[i] - 1;
            int resultY = iValuesByStep[stepId];
            int resultX = jValuesByStep[stepId];
            result.append(resultX).append(",").append(resultY);
            if (i != piecesAmount - 1) {
                result.append(" ");
            }
        }
        return result.toString();
    }
}
