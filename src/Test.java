public class Test {
    public static void main(String[] args) {
        for (int i = 0; i < 11; i++) {
            String path = "inputs/" + (i < 10 ? "0" + i : i) + ".txt";
            System.out.println(i);
            SolverManager.newInstance().solve(path);
        }
    }
}
